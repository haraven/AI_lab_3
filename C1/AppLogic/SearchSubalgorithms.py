from random import randint, uniform

from AppLogic.Chromosomes import BinaryChromosome


class EvolutionarySearches:
    @staticmethod
    def tournament_selection(tourney_size, chromosomes):
        if tourney_size <= 0 or len(chromosomes) == 0:
            return None

        sample = []
        while len(sample) < tourney_size:
            individual = chromosomes.pop(randint(0, len(chromosomes) - 1))
            sample.append(individual)

        return min(sample, key=lambda chromosome: chromosome.get_fitness())

    @staticmethod
    def binary_tournament(chromosomes):
        return EvolutionarySearches.tournament_selection(2, chromosomes)

    @staticmethod
    def single_point_cut_crossover(parent1, parent2, crossover_prob, cut_index):
        if uniform(0, 1) < crossover_prob:
            return None
        if cut_index >= len(parent1.get_binary_array()):
            return None

        child_arr = parent1.get_binary_array()[:cut_index + 1] + parent2.get_binary_array()[cut_index + 1:]
        child = BinaryChromosome(child_arr.copy(), parent1.get_name() + ", " + parent2.get_name())

        return child

    @staticmethod
    def strong_mutation(chromosome, mutation_chance):
        if chromosome is None:
            return None

        chromosome_representation = chromosome.get_binary_array()
        for i in range(len(chromosome_representation)):
            if uniform(0, 1) >= mutation_chance:
                chromosome_representation[i] = 0 if chromosome_representation[i] > 0 else 1

        return chromosome
