class TournamentChromosome:
    def __init__(self, fitness, name):
        self.__fitness = fitness
        self.__name = name

    def get_fitness(self):
        return self.__fitness

    def set_fitness(self, new_fitness):
        self.__fitness = new_fitness

        return self

    def get_name(self):
        return self.__name

    def set_name(self, new_name):
        self.__name = new_name

        return self

    def __eq__(self, other):
        if other is None:
            return False
        return self.__fitness == other.__fitness and self.__name == other.__name


class BinaryChromosome:
    def __init__(self, binary_arr, name):
        self.__binary_arr = binary_arr.copy()
        self.__name = name

    def get_binary_array(self):
        return self.__binary_arr

    def set_binary_array(self, new_binary_arr):
        self.__binary_arr = new_binary_arr

        return self

    def get_name(self):
        return self.__name

    def set_name(self, new_name):
        self.__name = new_name

    def __eq__(self, other):
        if other is None:
            return False
        return self.__name == other.__name and self.__binary_arr == other.__binary_arr
