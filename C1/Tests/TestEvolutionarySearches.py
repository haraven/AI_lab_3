import unittest
from unittest import TestCase

from AppLogic.Chromosomes import *
from AppLogic.SearchSubalgorithms import EvolutionarySearches


class TestEvolutionarySearches(TestCase):
    def test_binary_tournament(self):
        chromosomes = []
        chromosome = TournamentChromosome(3, "c1")
        chromosomes.append(chromosome)
        chromosome = TournamentChromosome(2, "c2")
        chromosomes.append(chromosome)
        chromosome = TournamentChromosome(5, "c3")
        chromosomes.append(chromosome)
        chromosome = TournamentChromosome(1, "c4")
        chromosomes.append(chromosome)
        chromosome = TournamentChromosome(7, "c5")
        chromosomes.append(chromosome)
        orig = chromosomes.copy()

        res = EvolutionarySearches.binary_tournament([])
        self.assertEqual(res, None)
        res = EvolutionarySearches.binary_tournament(chromosomes)
        self.assertEqual(res not in chromosomes, True)
        self.assertEqual(res in orig, True)

    def test_single_point_cut_crossover(self):
        parent1 = BinaryChromosome([1, 0, 1, 1, 0], "c1")
        parent2 = BinaryChromosome([1, 1, 0, 0, 0], "c2")
        crossover_prob = 0.8
        cut_index = 2

        res = EvolutionarySearches.single_point_cut_crossover(parent1, parent2, crossover_prob, cut_index)
        if res is not None:
            print("1st crossover:")
            print(res.get_binary_array())
            self.assertEqual(res.get_binary_array(), [1, 0, 1, 0, 0])

        parent1.set_binary_array([1, 0, 1, 1, 1])
        res = EvolutionarySearches.single_point_cut_crossover(parent1, parent2, crossover_prob, cut_index)
        if res is not None:
            print("2nd crossover:")
            print(res.get_binary_array())
            self.assertEqual(res.get_binary_array(), [1, 0, 1, 0, 0])

        crossover_prob = 0.0
        res = EvolutionarySearches.single_point_cut_crossover(parent1, parent2, crossover_prob, cut_index)
        self.assertIsNotNone(res)

        crossover_prob = 1.0
        res = EvolutionarySearches.single_point_cut_crossover(parent1, parent2, crossover_prob, cut_index)
        self.assertIsNone(res)

        cut_index = 0
        crossover_prob = 0.0
        parent1.set_binary_array([0] * 5)
        res = EvolutionarySearches.single_point_cut_crossover(parent1, parent2, crossover_prob, cut_index)
        self.assertEqual(res.get_binary_array(), [0, 1, 0, 0, 0])

    def test_strong_mutation(self):
        chromosome = BinaryChromosome([1, 0, 1, 1, 0], 0.1)
        mutation_prob = 0.0
        res = EvolutionarySearches.strong_mutation(chromosome, mutation_prob)
        self.assertIsNotNone(res)
        self.assertEqual(res.get_binary_array(), [0, 1, 0, 0, 1])

        mutation_prob = 1.0
        res = EvolutionarySearches.strong_mutation(chromosome, mutation_prob)
        self.assertEqual(res.get_binary_array(), chromosome.get_binary_array())

        mutation_prob = 0.0
        chromosome.set_binary_array([1, 1, 1, 1, 1])
        res = EvolutionarySearches.strong_mutation(chromosome, mutation_prob)
        self.assertEqual(res.get_binary_array(), [0] * 5)

        res = EvolutionarySearches.strong_mutation(None, mutation_prob)
        self.assertIsNone(res)


if __name__ == "__main__":
    unittest.main()
