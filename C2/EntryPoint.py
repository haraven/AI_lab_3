from Interface import Menu
import sys
import datetime
import time
from Searches.Informed.Local import EvolutionaryAlgorithm, AntColonyOptimization
from AppLogic.CrosswordGrid import CrosswordGrid
from Utils import CrosswordHelper


def print_grid(grid, dest):
    if dest != "console":
        file = open(dest)
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                file.write(str(grid[i][j]) + " ")
            file.write('\n')
        file.close()
    else:
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                print(str(grid[i][j]), end=" ")
            print()


def setup_UI(my_menu):
    if not issubclass(my_menu.__class__, Menu.Menu):
        raise TypeError("Cannot initialize " + my_menu + " as it is not a recognized menu.")

    item = Menu.Menu()
    item.append(Menu.MenuItem("- choose an input file for the crossword puzzle (file);"))
    item.append(Menu.MenuItem(
        "\t a crossword puzzle input file must be of the form:"
        "\n\n\t\033[91mtotal_rows, total_columns"
        "\n\tblack_square1_coord_x, black_square1_coord_y"
        "\n\t..."
        "\n\tblack_squaren_coord_x, black_squaren_coord_y\033[0m\n"))
    my_menu.append(item)
    item = Menu.MenuItem("- solve;")
    my_menu.append(item)
    item = Menu.MenuItem("- exit.")
    my_menu.append(item)

    return my_menu


def setup_solution_menu(my_menu):
    if not issubclass(my_menu.__class__, Menu.Menu):
        raise TypeError("Cannot initialize " + my_menu + " as it is not a recognized menu.")

    item = Menu.MenuItem("- evolutionary algorithm (ea);")
    my_menu.append(item)
    item = Menu.MenuItem("- ant colony optimization algorithm (aco).")
    my_menu.append(item)

    return my_menu


def handle_input(my_menu):
    grid = None
    solution_menu = setup_solution_menu(Menu.Menu())

    print("Welcome. Please choose one of the below options: ")
    while True:
        try:
            my_menu.parse()
            cmd = input("> ")
            if cmd == "file":
                filename = input("Enter a file to read from:\n> ")
                grid = CrosswordGrid(filename)
            elif cmd == "solve":
                if grid is None:
                    raise SyntaxError(
                        "Crossword puzzle is currently empty."
                        " You have to initialize it before trying to solve anything.\n")
                else:
                    dictionary = input("Enter the dictionary to use to solve the problem. "
                                       "Write the words inline, and separate them with a coma,"
                                       " like so: word1, word2, ...\n> ")
                    dictionary = dictionary.split(", ")

                    solution_menu.parse()
                    cmd = input("> ")
                    if cmd == "ea":
                        algorithm = EvolutionaryAlgorithm.EvolutionaryAlgorithm(dictionary)
                        pop_count = int(input("Enter the population size.\n> "))
                        sample_size = int(input("Enter the sample size (number of iterations).\n> "))
                        mutation_chance = float(input("Enter the chance to mutate upon crossover.\n> "))
                        crossover_chance = float(input("Enter the chance to crossover.\n> "))
                        crossover_point = int(
                            input("Enter the crossover point (algorithm uses single-point cut crossover).\n> "))
                        output_location = input("Where do you want the results to be outputted?"
                                                " Type \"console\" for console, and the name of a file,"
                                                " to write into a file.\n> ")
                        population = algorithm.generate_population(pop_count=pop_count, problem=grid)
                        start = time.time()
                        for x in range(sample_size):
                            # for individual in population:
                            #     res = CrosswordHelper.CrosswordHelper.get_slots_as_grid(individual,
                            #                                                             grid.get_row_count(),
                            #                                                             grid.get_col_count())
                            #     print_grid(res, output_location)
                            population = algorithm.iterate(population, mutation_chance,
                                                           crossover_chance=crossover_chance,
                                                           crossover_point=crossover_point)

                        res = CrosswordHelper.CrosswordHelper.get_slots_as_grid(algorithm.get_best_individual(population),
                                                                                grid.get_grid(),
                                                                                grid.get_row_count(),
                                                                                grid.get_col_count())
                        print_grid(res, output_location)

                        end = time.time()
                        start_datetime = datetime.datetime.fromtimestamp(start)
                        end_datetime = datetime.datetime.fromtimestamp(end)
                        print("Started at: " + str(start_datetime.strftime('%H:%M:%S')) + ". Ended at: " + str(
                            end_datetime.strftime('%H:%M:%S')) + ". Duration: " + str(
                            end_datetime - start_datetime) + ".")
                        grid = None
                    elif cmd == "aco":
                        # algorithm = AntColonyOptimization.AntColonyOptimization()
                        print("This feature has not yet been implemented.")
                    else:
                        raise SyntaxError("invalid command.")
            elif cmd == "exit":
                print("The application will now exit. Have a good one!")
                sys.exit(0)
            else:
                raise SyntaxError("Invalid command")
        except Exception as e:
            print("Unexpected error: " + str(e))


if __name__ == "__main__":
    menu = setup_UI(Menu.Menu())
    handle_input(menu)
