from AppLogic.CrosswordSlot import CrosswordSlot


class CrosswordGrid:
    def __init__(self, filename=None):
        if filename is None:
            return

        file = open(filename)
        line = file.readline()
        line = line.split(", ")
        self.__rows = int(line[0])
        self.__cols = int(line[1])
        self.__grid = [[None for x in range(self.__cols)] for x in range(self.__rows)]
        for line in file:
            line_arr = line.split(", ")
            row = int(line_arr[0])
            col = int(line_arr[1])
            self.__grid[row][col] = 1

        file.close()

    def set_filename(self, new_file):
        self.__init__(new_file)

        return self

    def get_free_slots_arr(self):
        slots = []
        for i in range(self.__rows):
            col_begin = 0
            for j in range(self.__cols):
                if self.__grid[i][j] == 1:
                    slots.append(CrosswordSlot(i, col_begin, j))
                    col_begin = j + 1
            slots.append(CrosswordSlot(i, col_begin, self.__cols - col_begin))

        return slots

    def get_grid(self):
        return self.__grid

    def get_row_count(self):
        return self.__rows

    def get_col_count(self):
        return self.__cols
