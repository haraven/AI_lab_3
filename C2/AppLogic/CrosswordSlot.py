class CrosswordSlot:
    def __init__(self, row, col_begin, max_len, val=""):
        self.__row = row
        self.__col_begin = col_begin
        self.__len = max_len
        self.__val = val

    def get_row(self):
        return self.__row

    def get_col_begin(self):
        return self.__col_begin

    def get_len(self):
        return self.__len

    def get_val(self):
        return self.__val

    def set_val(self, new_val):
        self.__val = new_val

        return self

    def __str__(self):
        return "{ " + str(self.__row) + ", " + str(self.__col_begin) + ", " + str(self.__len) + ", " + str(
            self.__val) + " }"

    def __repr__(self):
        return self.__str__()
