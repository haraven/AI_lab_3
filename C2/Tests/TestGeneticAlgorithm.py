from math import inf
from random import randint
from unittest import TestCase
from AppLogic.CrosswordGrid import CrosswordGrid
from AppLogic.CrosswordSlot import CrosswordSlot
from Searches.Informed.Local.GeneticAlgorithm import GeneticAlgorithm


class TestGeneticAlgorithm(TestCase):
    def test_crossover(self):
        grid = CrosswordGrid("G:\\University\\Artificial intelligence\\ai_lab_3\\C2\\Resources\\crossword_setup.txt")
        alg = GeneticAlgorithm(["range", "a", "bob", "ab", "ba", "ac", "ca"])
        population = alg.generate_population(pop_count=5, problem=grid)

        crossover_point = 0
        crossover_chance = 1.0
        rand_parent1 = population[randint(0, len(population) - 1)]
        rand_parent2 = population[randint(0, len(population) - 1)]
        child = alg.crossover(rand_parent1, rand_parent2,
                              crossover_chance=crossover_chance,
                              crossover_point=crossover_point)
        self.assertEqual(child[0], rand_parent1[0])
        self.assertEqual(child[1:], rand_parent2[1:])
        crossover_chance = 0.0
        child = alg.crossover(rand_parent1, rand_parent2,
                              crossover_chance=crossover_chance,
                              crossover_point=crossover_point)
        self.assertIsNone(child)

    def test_generate_population(self):
        grid = CrosswordGrid("G:\\University\\Artificial intelligence\\ai_lab_3\\C2\\Resources\\crossword_setup.txt")
        alg = GeneticAlgorithm(["range", "a", "bob", "ab", "ba", "ac", "ca"])
        population = alg.generate_population(pop_count=5, problem=grid)
        self.assertEqual(len(population), 5)
        slot = CrosswordSlot(0, 0, 0)
        self.assertEqual(population[0][0].__class__, slot.__class__)

    def test_mutate(self):
        grid = CrosswordGrid("G:\\University\\Artificial intelligence\\ai_lab_3\\C2\\Resources\\crossword_setup.txt")
        alg = GeneticAlgorithm(["range", "a", "bob", "ab", "ba", "ac", "ca"])
        population = alg.generate_population(pop_count=5, problem=grid)
        mutation_chance = 0.0
        individual = population[randint(0, len(population) - 1)]
        self.assertEqual(individual, alg.mutate(individual, mutation_chance))

    def test_fitness(self):
        grid = CrosswordGrid("G:\\University\\Artificial intelligence\\ai_lab_3\\C2\\Resources\\crossword_setup.txt")
        alg = GeneticAlgorithm(["range", "a", "bob", "ab", "ba", "ac", "ca"])
        population = alg.generate_population(pop_count=1, problem=grid)
        fitness = alg.fitness(population[0])
        self.assertTrue(0 <= fitness <= inf)

    def test_generate_individual(self):
        grid = CrosswordGrid("G:\\University\\Artificial intelligence\\ai_lab_3\\C2\\Resources\\crossword_setup.txt")
        alg = GeneticAlgorithm(["range", "a", "bob", "ab", "ba", "ac", "ca"])
        individual = alg.generate_individual(problem=grid)
        self.assertIsNotNone(individual)
        self.assertEqual(len(individual), 7)
        self.assertTrue(issubclass(individual[randint(0, len(individual) - 1)].__class__, CrosswordSlot))
