class CrosswordHelper:
    @staticmethod
    def get_slots_as_grid(slots, grid, grid_rows, grid_cols):
        for slot in slots:
            current_col = slot.get_col_begin()
            current_row = slot.get_row()
            current_val = slot.get_val()
            current_len = slot.get_len()
            if len(current_val) > len(grid[0]) - current_col:
                blanks_len = len(grid[0]) - current_len
                while blanks_len != 0:
                    if current_col >= len(grid[0]):
                        break
                    if grid[current_row][current_col] != 1:
                        grid[current_row][current_col] = "□"
                        current_col += 1
                        blanks_len -= 1
                    else:
                        grid[current_row][current_col] = "⬛"
                        break
            else:
                for letter in current_val:
                    if current_col >= len(grid[0]):
                        break
                    if grid[current_row][current_col] == 1:
                        grid[current_row][current_col] = "⬛"
                        break
                    else:
                        grid[current_row][current_col] = letter
                        current_col += 1
                if len(current_val) < current_len:
                    blanks_len = current_len - len(current_val)
                    while blanks_len != 0:
                        if grid[current_row][current_col] != 1:
                            grid[current_row][current_col] = "□"
                            current_col += 1
                            blanks_len -= 1
                        else:
                            grid[current_row][current_col] = "⬛"
                            break

        for i in range(grid_rows):
            for j in range(grid_cols):
                if grid[i][j] == 1:
                    grid[i][j] = "⬛"

        return grid
