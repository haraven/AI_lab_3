from abc import ABCMeta, abstractmethod

from Searches.SearchMethod import SearchMethod


class UninformedSearchMethod(SearchMethod):
    """Uninformed search method base class"""

    __metaclass__ = ABCMeta

    @abstractmethod
    def solve(self, tree, consistency_checker):
        pass

    @staticmethod
    @abstractmethod
    def parse(self, tree):
        pass
