from abc import ABCMeta, abstractmethod


class SearchMethod(object):
    """abstract search method class"""

    __metaclass__ = ABCMeta

    @abstractmethod
    def solve(self, tree, consistency_checker):
        pass

    @staticmethod
    @abstractmethod
    def parse(self, tree):
        pass
