from Utils.PriorityQueue import PriorityQueue

from Searches.Informed.BFS.InformedSearchMethod import InformedSearchMethod


class BestFirstSearch(InformedSearchMethod):
    """Best first search base class"""

    def __init__(self, f):
        self.__container = PriorityQueue(min, f)

    def solve(self, tree, consistency_checker):
        visited = {}
        self.__container.append(tree.get_root())
        while len(self.__container) > 0:
            node = self.__container.pop()
            val = node.get_value()
            if consistency_checker.is_solution(val):
                return val
            elif val not in visited:
                visited[val] = True
                nodes = val.setup_children()
                node.add_children(nodes)
                self.__container.extend(nodes)

        return None

    @staticmethod
    def parse(self, tree):
        pass
