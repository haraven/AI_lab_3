from abc import ABCMeta

from Searches.Informed.BFS.InformedSearchMethod import InformedSearchMethod


class LocalSearch(InformedSearchMethod):
    __metaclass__ = ABCMeta

    def solve(self, tree, consistency_checker):
        pass

    @staticmethod
    def parse(self, tree):
        pass
