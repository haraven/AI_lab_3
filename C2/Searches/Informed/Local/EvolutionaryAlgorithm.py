from math import inf
from random import randint, uniform

from Searches.Informed.Local.BeamLocalSearch import BeamLocalSearch


class EvolutionaryAlgorithm(BeamLocalSearch):
    def __init__(self, words=None):
        self.__words = words

    # single point crossover
    def crossover(self, parent1, parent2, **kwargs):
        crossover_index = kwargs["crossover_point"]
        crossover_prob = kwargs["crossover_chance"]
        if uniform(0, 1) < crossover_prob:
            child = parent1[:crossover_index + 1] + parent2[crossover_index + 1:]
            return child
        return None

    def iterate(self, p, mutation_prob, **kwargs):
        crossover_index = kwargs["crossover_point"]
        crossover_prob = kwargs["crossover_chance"]

        index1 = randint(0, len(p) - 1)
        index2 = randint(0, len(p) - 1)
        if index1 != index2:
            child = self.crossover(p[index1], p[index2],
                                   crossover_point=crossover_index,
                                   crossover_chance=crossover_prob)
            if child is not None:
                child = self.mutate(child, mutation_prob)
            parent1_fitness = self.fitness(p[index1])
            parent2_fitness = self.fitness(p[index2])
            child_fitness = self.fitness(child)
            if parent1_fitness > parent2_fitness and parent1_fitness > child_fitness:
                p[index1] = child
            elif parent1_fitness <= parent2_fitness and parent2_fitness > child_fitness:
                p[index2] = child

        return p

    def generate_population(self, **kwargs):
        population_count = kwargs["pop_count"]
        problem = kwargs["problem"]
        return [self.generate_individual(problem=problem) for x in range(population_count)]

    def mutate(self, individual, mutation_prob, **kwargs):
        if uniform(0, 1) < mutation_prob:
            random_index1 = randint(0, len(individual) - 1)
            random_index2 = randint(0, len(individual) - 1)
            val1 = individual[random_index1].get_val()
            val2 = individual[random_index2].get_val()
            individual[random_index1].set_val(val2)
            individual[random_index2].set_val(val1)

        return individual

    def fitness(self, individual):
        if individual is None:
            return inf
        diff_sum = 0

        seen = []
        for slot in individual:
            if slot.get_val() in seen:  # slots that contain duplicate words are not valid
                return inf
            seen.append(slot.get_val())
            diff_sum += abs(len(slot.get_val()) - slot.get_len())

        return diff_sum

    def generate_individual(self, **kwargs):
        problem = kwargs["problem"]
        words = self.__words.copy()

        problem_initial_grid = problem.get_free_slots_arr()
        pos = 0
        while not (len(words)) == 0:
            word = words.pop(randint(0, len(words) - 1))
            problem_initial_grid[pos].set_val(word)
            pos += 1

        return problem_initial_grid

    def get_best_individual(self, p):
        best = p[0]
        for index in range(1, len(p)):
            if self.fitness(best) > self.fitness(p[index]):
                best = p[index]

        return best
