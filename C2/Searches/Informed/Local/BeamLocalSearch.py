from abc import ABCMeta

from Searches.Informed.Local.LocalSearch import LocalSearch


class BeamLocalSearch(LocalSearch):
    __metaclass__ = ABCMeta